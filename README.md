# Street-Light-Scene (Demo)

Hi there EtowTheOfficeCat Here! Today i´ll present you this beautifull little Light Scene that I have created!

![](https://media.giphy.com/media/jUiko9b4A2rfNAB3eh/giphy.gif)

**GameBuild Dowload**

https://www.dropbox.com/s/01m3xdl1mthiv9j/StreetLightScene.zip?dl=0

First light scene that I have created in Unity. Theme is a town street during a dark night lit by street lights and the buildings around it. I have used my own assets for the buildings in the street.[(Artstation)](https://www.artstation.com/paulmabon/albums/all)
Scene can also be view in VR and looks as awesome if not even better here little preview [(youtube)](https://www.youtube.com/watch?v=PobfikjODDI&feature=youtu.be)

![Alt Text](https://i.imgur.com/lZumq7R.png)

Used two spot lights (for the streetlights) and one point light for the lamp at the restaurant. The windows of the Houses as well as the spotlight of the billboards are all quad planes with an emissive material. 

![Alt Text](https://i.imgur.com/FUXgBkp.png)

